import { useState, useEffect } from 'react';
import { Layout } from '../../components/layouts'
import { NoContent } from '../../components/ui';
import { localFavourites } from '../../utils';
import { Favourites } from '../../components/ui/Favourites/Favourites';

const FavouritesPage = () => {
    const [favouritePokemon, setFavouritePokemons] = useState<number[]>([])
    
    useEffect(() => {
        setFavouritePokemons(localFavourites.pokemons);
    }, [])
    

    return (
        <Layout title="Pokémon - Favourites">
            {
                !favouritePokemon.length ? 
                    <NoContent text="There aren't any Pokemon in Favourites yet"/> :
                    <Favourites pokemon={favouritePokemon}/>
            }
        </Layout>
    )
}

export default FavouritesPage;