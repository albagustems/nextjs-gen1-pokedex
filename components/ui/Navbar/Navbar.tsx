import { Link, Spacer, Text, useTheme } from '@nextui-org/react'
import Image from 'next/image';
import NextLink from 'next/link'
import React from 'react'

export const Navbar = () => {
    const { theme } = useTheme();
    
    return (
        <div style={{ 
            display: 'flex',
            width: '100%',
            alignItems: 'center',
            padding: '0 20px',
            backgroundColor: theme?.colors.gray900.value
        }}>
            <Image src="https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/6.png" alt="App Icon" width={70} height={70}/>
            <NextLink href="/" passHref>
                <Link>
                    <Text color='white' h2>P</Text>
                    <Text color='white' h3>okémon</Text>
                </Link>
            </NextLink>
            <Spacer css={{ flex: 1 }}/>
            <NextLink href="/favourites" passHref>
                <Link>
                    <Text color='white' >Favorites</Text>
                </Link>
            </NextLink>
        </div>
    )
}
