import { Container, Text, Image } from '@nextui-org/react'
import React from 'react'

interface Props {
    text: string
}

export const NoContent: React.FC<Props> = ({ text }) => {
    return (
        <Container css={{ 
            display: 'flex', 
            flexDirection: 'column', 
            height: 'calc(100vh - 100px)',
            alignItems: 'center',
            justifyContent: 'center',
            alignSelf: 'center'
            }}>
                <Text css={{ textAlign: 'center' }} h2>{text}</Text>
                <Image 
                    src='https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/6.png' 
                    alt='Favourite Pokemon'
                    width={250}
                    height={250}
                    css={{ 
                        opacity: 0.2
                    }} />
        </Container>
    )
}
