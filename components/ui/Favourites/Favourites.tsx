import { Grid } from '@nextui-org/react'
import React from 'react'
import { FavouriteCard } from '../FavouriteCard/FavouriteCard'

interface Props {
    pokemon: number[]
}

export const Favourites: React.FC<Props> = ({ pokemon }) => {
    return (
        <Grid.Container gap={2}>
            {
                pokemon.map(id => (
                    <FavouriteCard key={id} pokemonId={id} />
                ))
            }
        </Grid.Container>
    )
}
