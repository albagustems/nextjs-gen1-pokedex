import Head from 'next/head'
import React from 'react'
import { Navbar } from '../ui';

interface Props {
    children: React.ReactNode,
    title?: String,
}

const origin = (typeof window === 'undefined') ? '' : window.location.origin;

export const Layout: React.FC<Props> = ({ children, title }) => {


    return (
        <>
            <Head>
                <title>{ title || 'Pokemon App'}</title>
                <meta name="author" content="Alba Gustems"/>
                <meta name="description" content={`Information about ${ title }`}/>
                <meta name="keywords" content={`${title}, pokemon, pokedex`}/>

                <meta property="og:title" content={`Pokedex Information ${title ? 'about ' + title : ''}`} />
                <meta property="og:description" content={`Pokedex page information ${title ? 'about ' + title : ''}`} />
                <meta property="og:image" content={`${origin}/images/banner.png`} />
            </Head>
            <Navbar/>
            <main style={{ padding: '0px 20px'}}>
                { children }
            </main>
        </>
    )
}
